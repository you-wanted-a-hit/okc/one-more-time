FROM python:3.8-slim-buster

ENV SUPERCRONIC_VERSION=0.1.12 \
    SUPERCRONIC_SHA1SUM=048b95b48b708983effb2e5c935a1ef8483d9e3e

RUN apt-get update && apt-get install -y \
        wget \
    && rm -rf /var/lib/apt/lists/*

RUN wget -O /usr/local/bin/supercronic -q "https://github.com/aptible/supercronic/releases/download/v${SUPERCRONIC_VERSION}/supercronic-linux-amd64" \
 && echo "${SUPERCRONIC_SHA1SUM}  /usr/local/bin/supercronic" | sha1sum -c - \
 && chmod +x /usr/local/bin/supercronic

WORKDIR /one-more-time

COPY requirements.txt ./

RUN pip install -r requirements.txt

COPY . ./

CMD ["/usr/local/bin/supercronic", "-passthrough-logs", "./crontab"]