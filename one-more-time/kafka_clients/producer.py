import json
from kafka import KafkaProducer

def serialize_value(msg):
    '''Serializes the responses
        Helps making the messages valid for Kafka \
            before producing them into the cluster.
    '''
    if msg['status'] == 200:
        if msg['endpoint'] == 'search':
            msg['response'] = msg['response'][0].serialize()
        else:
            msg['response'] = msg['response'].serialize()
    return msg

class Producer(KafkaProducer):
    ''' Custom Kafka Producer class
        
        Inheriting from the KafkaProducer class \
            and configuring the creation \
            of the producer.
    '''
    
    def __init__(self, host: str, port: int):
        '''Constructor for the producer

        Parameters
        ----------
        host : str
            The IP or domain of a Kafka Broker

        port: int
            The port number the Kafka Broker listens at.
        '''
        super().__init__(
            bootstrap_servers=[f'{host}:{port}'],
            value_serializer= lambda msg: json.dumps(msg).encode('utf-8'),
            key_serializer= lambda key: key.encode('utf-8')
        )
    
