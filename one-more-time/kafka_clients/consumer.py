import json
from kafka import KafkaConsumer
from log import log

class Consumer(KafkaConsumer):
    ''' Custom Kafka Consumer class
        
        Inheriting from the KafkaConsumer class \
            and configuring the creation \
            of the consumer.
    '''
    def __init__(self, host: str, port: int, topics: str, consumer_group: str):
        '''Constructor for the Consumer class
            
            Creates new consumer object using the supplied parameters

            Parameters
            ----------
            host : str
                The IP or domain of a Kafka Broker.

            port: int
                The port number the Kafka Broker listens at.

            topics : str
                The topics the consumer will listen to. 
                
            consumer_group: str
                The consumer group the consumer will live under.

        '''
        super().__init__(
            topics,
            bootstrap_servers=[f'{host}:{port}'],
            auto_offset_reset='earliest',
            enable_auto_commit=True,
            auto_commit_interval_ms=1000,
            max_poll_records=1,
            max_poll_interval_ms=1000,
            group_id=consumer_group,
            value_deserializer=lambda msg: json.loads(msg.decode('utf-8')),
            key_deserializer=lambda key: key.decode('utf-8')
        )
        self.logger = log.get_logger('CONSUMER')
    
    def poll_one(self):
        for message in self:
            try:
                self.commit()
            except Exception as e:
                self.logger.exception(e)
            
            return message.value

    
