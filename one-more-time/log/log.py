import logging

'''Custom Logging Module'''

def get_logger(name: str, level=logging.INFO) -> logging.Logger:
    '''Creates a logger with the specified name and level
    
        The logger will log to `stdout`. 
       
        Parameters
        ----------
        name: str
            The logger's name.
        
        level: logging.INFO, optional
            The logging level (DEBUG, INFO, WARN, etc.)

        Returns:
        ---
        logging.Logger
            Logger object.
    '''

    # Setting the format
    formatter = logging.Formatter(fmt='[ %(asctime)s ] %(levelname)-4s %(name)-4s {%(module)s:%(funcName)s:%(lineno)s} %(message)s')

    # Creating an stdout handler
    handler = logging.StreamHandler()
    handler.setFormatter(formatter)

    # Creating the logger
    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(handler)
    
    return logger
