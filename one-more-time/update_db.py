from decouple import config
from kafka_clients.consumer import Consumer
from kafka_clients.producer import Producer
from log import log

logger = log.get_logger('UPDATE-DB')

logger.debug('Creating producer')
producer = Producer(host=config('KAFKA_HOST_NAME'), port=config('KAFKA_HOST_PORT'))
logger.debug('Succesfully created producer')

logger.debug('Creating consumer for each service in update flow')
consumers = {}
for service in ['wall', 'curls']:
    logger.debug(f'Creating {service} consumer')
    consumers[service] = Consumer(host=config('KAFKA_HOST_NAME'), \
        port=config('KAFKA_HOST_PORT'), \
        topics=f'{service}.responses.crons',
        consumer_group=config("KAFKA_UPDATE_DB_CONSUMER_GROUP"))
    logger.debug(f'Created {service} consumer')
logger.debug('Created consumer for each service in update flow')


# Query DB for all partials
logger.info('Querying DB for partials')
db_query = {
    'crons': True,
    'type': 'partials',
    'object': None
}
producer.send(topic='wall.requests', value=db_query, key='GET')
partials = consumers['wall'].poll_one()

# logger.debug(partials)

# logger.debug(f"Tracks: {partials['tracks']}")
# logger.debug(f"Albums: {partials['albums']}")
# logger.debug(f"Artists: {partials['artists']}")


# Iterate each list of partials
for _type in partials.keys():
    singular_type = _type[:-1:]
    logger.info(f'Iterating over {_type}')
    for _id in partials[_type]:
        # Get full asset from Curls
        curls_query = {
            'crons': True,
            '_id': _id
        }
        logger.info(f'Sending request to curls to get {singular_type} {_id}')
        producer.send(topic='curls.requests', value=curls_query, key=singular_type)
        
        obj = consumers['curls'].poll_one()
        logger.debug(obj)
        response = obj['response'] 
        logger.debug(response)

        # save full asset in DB
        if (singular_type =='track' and not response.get('audio_features')) or \
            obj['status'] != 200:
            logger.warn('TRACK DOES NOT HAVE AUDIO FEATURES or error occured')
            logger.warn('Track was skipped, will try again next run')
            continue

        if singular_type == 'track' or singular_type == 'album':
            name = response['title'] 
        elif singular_type == 'artist':
            name = response['name'] 

        logger.info(f"Saving {singular_type} {name} : {_id} in DB")
        db_post ={
            'type': singular_type,
            'object': response
        }
        producer.send(topic='wall.requests', value=db_post, key='POST')

logger.info('DONE')