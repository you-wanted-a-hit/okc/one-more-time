from decouple import config
from kafka_clients.consumer import Consumer
from kafka_clients.producer import Producer
from log import log
import csv
from datetime import datetime

logger = log.get_logger('UPDATE-MODELS')

logger.debug('Creating producer')
producer = Producer(host=config('KAFKA_HOST_NAME'), port=config('KAFKA_HOST_PORT'))
logger.debug('Succesfully created producer')

logger.debug('Creating consumer for each service in update flow')
consumers = {}
for service in ['hot-thing', 'wall', 'curls', 'insight']:
    logger.debug(f'Creating {service} consumer')
    
    topic = f'{service}.responses'
    if service == 'wall' or service == 'curls':
        topic += '.crons'
    logger.debug(topic)
    
    consumers[service] = Consumer(host=config('KAFKA_HOST_NAME'), \
        port=config('KAFKA_HOST_PORT'), \
        topics=topic,
        consumer_group=config("KAFKA_UPDATE_MODELS_CONSUMER_GROUP"))
    logger.debug(f'Created {service} consumer')
logger.debug('Created consumer for each service in update flow')

# Getting the latest chart
charts_query = {
    'name': 'hot-100'
}
logger.info('Requesting chart from hot-thing')
producer.send(topic='hot-thing.requests', value=charts_query, key='chart')
chart = consumers['hot-thing'].poll_one()

logger.debug(chart)

final_results = []

for entry in chart['response']['entries']:
    logger.info('Searching for track in DB')
    # Searching for track in DB
    db_query = {
        'crons': True,
        'type': 'track',
        'object': {
            'title': entry['title'],
            'artist': entry['artist']
        }
    }
    producer.send(topic='wall.requests', value=db_query, key='GET')
    track = consumers['wall'].poll_one()
    
    logger.debug(track)
    if not track:
        logger.info('Track does not exist in DB')
        logger.info('Searching for track in Spotify')
        search_spotify_query = {
            'crons': True,
            'query': f"{entry['title']}",
            'type': 'track',
            'limit': 1
        }
        logger.debug('sending request to curls')
        producer.send(topic='curls.requests', value=search_spotify_query, key='search')
        logger.debug('waiting for results')
        search_result = consumers['curls'].poll_one()

        logger.debug(search_result)
        if search_result:
            logger.info('Track exists in Spotify')
            logger.info(f"Requesting {search_result['response']['_id']} from Spotify")

            track_query = {
                'crons': True,
                '_id': search_result['response']['_id']
            }
            producer.send(topic='curls.requests', value=track_query, key='track')
            track = consumers['curls'].poll_one()
            logger.debug(track)
            track = track['response'] 
            logger.debug(track)

            logger.info('Saving track in DB')
            db_query = {
                'type': 'track',
                'object': track
            }
            producer.send(topic='wall.requests', value=db_query, key='POST')
        else:
            logger.info("=== SKIPPED ===")
            logger.info(f"Cannot find track {entry['title']} in Spotify ")
            logger.info("=== SKIPPED ===")
            continue

    if track.get('audio_features'):
        result = {
            'date': datetime.strptime(chart['response']['date'], '%Y-%m-%d').strftime('%m/%d/%Y'),
            'position': entry['rank'],
            'title': track['title'],
            'artist': track['artists'][0]['name'],
            'spotify_id': track['_id'],
            'is_new': entry['is_new'],
            'last_pos': entry['last_pos'],
            'peak_pos': entry['peak_pos'],
            'weeks': entry['weeks'],
            'danceability': track['audio_features']['danceability'],
            'energy': track['audio_features']['energy'],
            'key': track['audio_features']['key'],
            'loudness': track['audio_features']['loudness'],
            'mode': track['audio_features']['mode'],
            'speechiness': track['audio_features']['speechiness'],
            'acousticness': track['audio_features']['acousticness'],
            'instrumentalness': track['audio_features']['instrumentalness'],
            'liveness': track['audio_features']['liveness'],
            'valence': track['audio_features']['valence'],
            'tempo': track['audio_features']['tempo'],
        }
        
        logger.debug(result)
        final_results.append(result)


date = chart['response']['date']
logger.info('Updating model')
logger.info(f"Saving data to new CSV file named {date}")

keys = final_results[0].keys()
logger.debug(keys)
with open(f'{config("SHARED_VOLUME_PATH")}/{date}.csv', 'w', newline='')  as output_file:   
    dict_writer = csv.DictWriter(output_file, keys)
    dict_writer.writeheader()
    dict_writer.writerows(final_results)

logger.info('Notifing model to update')
update_request = {
    'path': f'{config("SHARED_VOLUME_PATH")}/{date}.csv'
}
producer.send(topic='insight.requests', value=update_request, key='train')

logger.info("DONE")